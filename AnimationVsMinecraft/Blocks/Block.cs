﻿using AnimationVsMinecraft.Blocks.SimpleBlocks;
using AnimationVsMinecraft.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationVsMinecraft.Blocks
{
    public static class Block
    {
        public static int blockSize = 20;
        public static SimpleBlock GRASS = new SimpleBlock(1);
        public static SimpleBlock WOOD = new SimpleBlock(2);
        public static SimpleBlock LEAVES = new SimpleBlock(3);
        public static SimpleBlock STONE = new SimpleBlock(4);

        public static IBlock GetBlock(int id)
        {
            switch (id)
            {
                case 0:
                    return null;
                case 1:
                    return GRASS;
                case 2:
                    return WOOD;
                case 3:
                    return LEAVES;
                case 4:
                    return STONE;
                default:
                    throw new UnimplementedBlockException();
            }
        }

    }
}
