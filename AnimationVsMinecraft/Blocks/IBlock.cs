﻿using AnimationVsMinecraft.WorldHandling;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AnimationVsMinecraft.Blocks
{
    public interface IBlock
    {
        void draw(SpriteBatch spriteBatch, World world, int x, int y);
    }
}