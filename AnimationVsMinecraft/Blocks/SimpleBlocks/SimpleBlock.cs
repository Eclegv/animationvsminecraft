﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimationVsMinecraft.WorldHandling;
using AnimationVsMinecraft.Exceptions;
using Microsoft.Xna.Framework.Graphics;

namespace AnimationVsMinecraft.Blocks.SimpleBlocks
{
    public class SimpleBlock : IBlock
    {
        int textureId;

        public SimpleBlock(int textureId)
        {
            this.textureId = textureId;
        }

        public string getBlockType()
        {
            switch (textureId)
            {
                case 1:
                    return "GRASS";
                case 2:
                    return "WOOD";
                case 3:
                    return "LEAVES";
                case 4:
                    return "STONE";
                default:
                    throw new UnimplementedBlockException();
            }
        }

        public void clear(int x, int y)
        {
            throw new NotImplementedException();
        }

        public void draw(SpriteBatch spriteBatch, World world, int x, int y)
        {
            switch (textureId)
            {
                case 1:
                    world.drawBlock(spriteBatch, Resources.grassBlock, x, y);
                    break;
                case 2:
                    world.drawBlock(spriteBatch, Resources.woodBlock, x, y);
                    break;
                case 3:
                    world.drawBlock(spriteBatch, Resources.leavesBlock, x, y);
                    break;
                case 4:
                    world.drawBlock(spriteBatch, Resources.stoneBlock, x, y);
                    break;
            }
        }
    }
}
