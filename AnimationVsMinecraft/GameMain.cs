﻿using AnimationVsMinecraft.WorldHandling;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationVsMinecraft
{
    public class GameMain
    {
        World world;
        int windowWidht;
        int windowHeight;

        public GameMain(int windowWidht, int windowHeight)
        {
            this.windowWidht = windowWidht;
            this.windowHeight = windowHeight;
            world = new World(this.windowWidht, this.windowHeight);
        }

        public void draw(SpriteBatch spriteBatch)
        {
            world.draw(spriteBatch);
        }

        public void update()
        {

        }
    }
}
