﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationVsMinecraft
{
    public static class Resources
    {

        //BLOCKS
        public static Texture2D grassBlock;
        public static Texture2D woodBlock;
        public static Texture2D leavesBlock;
        public static Texture2D stoneBlock;

        //ITEMS

        //OTHERS
        public static Texture2D background;

        public static void LoadContent(ContentManager content)
        {
            LoadBlocks(content);
            LoadOthers(content);
        }

        private static void LoadBlocks(ContentManager content)
        {
            grassBlock = content.Load<Texture2D>("Textures/Blocks/grass");
            woodBlock = content.Load<Texture2D>("Textures/Blocks/wood");
            leavesBlock = content.Load<Texture2D>("Textures/Blocks/leaves");
            stoneBlock = content.Load<Texture2D>("Textures/Blocks/stone");
        }

        private static void LoadItems(ContentManager content)
        {
            throw new NotImplementedException();
        }

        private static void LoadOthers(ContentManager content)
        {
            background = content.Load<Texture2D>("Textures/BackGround/background1600900");
        }
    }
}
