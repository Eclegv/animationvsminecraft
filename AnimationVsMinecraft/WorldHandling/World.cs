﻿using AnimationVsMinecraft.Blocks;
using AnimationVsMinecraft.Blocks.SimpleBlocks;
using AnimationVsMinecraft.WorldHandling.WorldGeneration;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationVsMinecraft.WorldHandling
{
    public class World
    {
        public int widthInBlocks;
        public int heightInBlocks;

        char[] id;
        char[] metadata;

        public World(int windowWidth, int windowHeight)
        {
            widthInBlocks = windowWidth / Block.blockSize;
            heightInBlocks = windowHeight / Block.blockSize;

            id = new char[widthInBlocks * heightInBlocks];
            metadata = new char[widthInBlocks * heightInBlocks];

            Array.Clear(id, 0, id.Length);
            Array.Clear(metadata, 0, metadata.Length);

            WorldGenerator wG = new WorldGenerator(this, new TreesGenerator(this, widthInBlocks, 3, 9));
            wG.generate();
        }

        public int getBlockIdFromCoord(int x, int y)
        {
            return id[x + (y * this.widthInBlocks)];
        }

        public int getMetadataFromCoord(int x, int y)
        {
            return metadata[x + (y * this.widthInBlocks)];
        }

        public void setBlockAndMetadataAt(char iD, char metaData, int x, int y)
        {
            id[x + (y * widthInBlocks)] = iD;
            metadata[x + (y * widthInBlocks)] = metaData;
        }

        public int getHigherGroundBlock(int x)
        {
            for (int y = heightInBlocks-1; y > 0; y--)
            {
                SimpleBlock actBlock = (SimpleBlock)Block.GetBlock(id[x + (y * widthInBlocks)]);
                if (actBlock != null && actBlock.getBlockType().Equals("GRASS"))
                {
                    return y;
                }
            }
            return heightInBlocks-1;
        }

        public void removeBlock(int x, int y)
        {
            throw new NotImplementedException();
        }

        public void drawBlock(SpriteBatch spriteBatch, Texture2D blockTex, int x, int y)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(blockTex, new Rectangle(x*Block.blockSize, y*Block.blockSize, Block.blockSize, Block.blockSize), Color.White);
            spriteBatch.End();
        }

        public void draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < widthInBlocks; x++)
            {
                for (int y = 0; y < heightInBlocks; y++)
                {
                    SimpleBlock act = (SimpleBlock)Block.GetBlock(id[x + (y * widthInBlocks)]);
                    if (act != null)
                    {
                        act.draw(spriteBatch, this, x, y);
                    }
                }
            }
        }

        public void update()
        {
            throw new NotImplementedException();
        }

    }
}
