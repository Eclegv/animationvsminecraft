﻿using AnimationVsMinecraft.Blocks;
using AnimationVsMinecraft.Blocks.SimpleBlocks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationVsMinecraft.WorldHandling.WorldGeneration
{
    public class TreesGenerator
    {
        World world;
        int treeNumber;
        int treeMinHeight;
        int treeMaxHeight;


        public TreesGenerator(World world, int treeNumber, int treeMinHeight, int treeMaxHeight)
        {
            this.world = world;
            this.treeNumber = treeNumber;
            this.treeMinHeight = treeMinHeight;
            this.treeMaxHeight = treeMaxHeight;
        }

        public void generateTrees()
        {
            Random r = new Random();
            for (int i = 0; i < treeNumber; i++)
            {
                int x = r.Next(0, world.widthInBlocks-1);
                int y = world.getHigherGroundBlock(x);
                int height = r.Next(treeMinHeight, treeMaxHeight);
                if(!treeIsNear(x, y, height))
                    generateTree(x, y-1, height);

            }
        }

        public bool treeIsNear(int x, int y, int heightTree)
        {

            for (int yp = y; yp > y - heightTree && yp < world.heightInBlocks; yp--)
            {
                for (int xp = x - 4; xp <= x + 4 && xp < world.widthInBlocks; xp++)
                {
                    SimpleBlock actBlock = (SimpleBlock)Block.GetBlock(world.getBlockIdFromCoord(xp, yp));
                    if (actBlock != null && actBlock.getBlockType().Equals("WOOD"))
                        return true;
                }
            }
            return false;
        }

        private void generateTree(int x, int y, int height)
        {
            for (int h = y; h > y - height; h--)
            {
                world.setBlockAndMetadataAt((char)2, (char)0, x, h);
            }
            generateLeaves(x, y-height);
        }

        public void generateLeaves(int x, int y)
        {
            int R = 2;
            for (int theta = -90; theta < 90; theta++)
            {
                for (int phi = 0; phi < 360; phi++)
                {
                    int xP = (int)(x + (Math.Cos(theta) * R));
                    int yP = (int)(y+1 + Math.Sin(phi) * R);

                    SimpleBlock actBlock = (SimpleBlock)Block.GetBlock(world.getBlockIdFromCoord(xP, yP));
                    if (actBlock == null || !actBlock.getBlockType().Equals("WOOD"))
                        world.setBlockAndMetadataAt((char)3, (char)0, xP, yP);
                }
            }
            //world.setBlockAndMetadataAt((char)3, (char)0, x, y);
        }
    }
}
