﻿using AnimationVsMinecraft.Blocks;
using AnimationVsMinecraft.Blocks.SimpleBlocks;
using AnimationVsMinecraft.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimationVsMinecraft.WorldHandling.WorldGeneration
{
    public class WorldGenerator
    {
        World world;
        TreesGenerator tG;

        public WorldGenerator(World world, TreesGenerator tG)
        {
            this.world = world;
            this.tG = tG;
        }

        public void generate()
        {
            generateGround();
            tG.generateTrees();
        }

        private void generateGround()
        {
            float inc = 0.01f;
            float xoff = 0.0f;
            PerlinNoiseHelper PNH = new PerlinNoiseHelper(new Random().Next(0, 2147000000)*new Random().Next(0, 1548496453));
            for (int x = 0; x < world.widthInBlocks; x++)
            {
                double y = world.heightInBlocks - 1 - Math.Abs(PNH.Noise(xoff, 0.0, 0.0)) * (world.heightInBlocks/3);
                world.setBlockAndMetadataAt((char)1, (char)0, x, (int)y);
                xoff += inc;
                fillUnderGrass(x, (int)y);
            }
        }

        private void fillUnderGrass(int x, int y)
        {
            for (int yp = y+1; yp < world.heightInBlocks; yp++)
            {
                world.setBlockAndMetadataAt((char)4, (char)0, x, (int)yp);
            }
        }
    }
}
